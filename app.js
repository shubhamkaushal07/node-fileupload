

process.env.NODE_ENV = 'localDevelopment';
config = require('config');
dbConnection = require('./routes/dbConnection');
constant = require('./routes/constant');

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');

var app = express();

var multer = require('multer');
var storage = multer.diskStorage({
    destination: function(req,res,cb){
       cb(null,'./public/images')},
    filename: function(req,file,cb){
        var ext=file.mimetype.split('/')
        cb(null,file.fieldname + 'image'+ Date.now()+'.'+ext[1])
    }
});

var upload=multer({storage : storage});
var routes = require('./routes/index');
//var users = require('./routes/users');
var fileupload = require('./routes/fileupload');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/test', routes);
//app.use(multer({dest:'./public/images/',filename:'picture.gif'}).single('file_upload'));
app.post('/file_upload',upload.single('file_upload'),fileupload);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        console.log(err);
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    console.log(err);
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});

//module.exports = app;
